/*
 * 	Memorizer
 *
 *	by H
 *
 */

/* Info

Set up connection:




Get from collection;
collection.find().toArray(function(err, docs) {
  // Do something...
	res.send(docs)
})

Set to collection:
collection.insert({name: 'taco', tasty: true}, function(err, result) {
 })


Set/get session stuff:

app.get('/', function(req, res, next) {
  if (req.session.views) {
    req.session.views++
    res.setHeader('Content-Type', 'text/html')
    res.write('<p>views: ' + req.session.views + '</p>')
    res.write('<p>expires in: ' + (req.session.cookie.maxAge / 1000) + 's</p>')
    res.end()
  } else {
    req.session.views = 1
    res.end('welcome to the session demo. refresh!')
  }
})


Socket.io

SERVER-SIDE
io.on('connection', socket => {
  socket.emit('request', … ); // emit an event to the socket
  io.emit('broadcast', … ); // emit an event to all connected sockets
  socket.on('stuff', () => { … }); // listen to an event
});





CLIENT-SIDE

<script src="/socket.io/socket.io.js"></script>
<script>
  const socket = io('http://localhost');
</script>





*/

// Config variables

const developing = false;

const connectionString = "mongodb+srv://monkeySpaghettiCheese:monkeySpaghettiCheese@memorizer-otqic.mongodb.net/test?retryWrites=true&w=majority";


/* Requires */
const bodyParser = require('body-parser');
const express = require('express');
const mongodb = require('mongodb');
const socketio = require('socket.io');
//const session = require("express-session");


/* Ordinary variables */
var collection;
var database;
var highscores = [];


var port = process.env.PORT;
if (port === undefined) {
	port = 80;
}	

/* Set up modules */
var app = express();
app.use(bodyParser.urlencoded({ extended: false }))




/* Set up Mongo & associated variables */

if (!developing) {

var MongoClient = mongodb.MongoClient;

	function afterConnect(err, db) {
		console.log("Connected correctly to server");
	if (!!err) {
	  	console.log('STOP! CONNECTION TO SERVER ABORTED! DATA: '+err);
	  	throw err;
	  }
	  database = db.db('documents');
	  collection = database.collection('highscores');
	  
	  getHighScores();
	}


MongoClient.connect(connectionString, afterConnect);

}


function getHighScores(a) {
	if (developing) {
		highscores = [
			{
				'_id':'1208938973894170590215097',
				'name':'hugh',
				'score':"1800"
			},
			
			{
				'_id':'2456571827082359623057801',
				'name':'tester records',
				'score':"9280"
			},
			
			{
				'_id':'5361758972365018672349578',
				'name':'john "infinity stone" smith',
				'score':"99999999"
			}
		];
		return;
	}
  collection.find({}).toArray(function(err, docs) {

    if (err) console.error(err);
    
    highscores = docs;
	  if (a) {
	io.emit('hereAreTheScores', highscores);
	  }
  });      

}


/* Startup */
var server = app.listen(port, () => {
  console.log('Started server at port '+port);
});

/* Set up socket.io stuff */
const io = socketio.listen(server);

io.on('connect', (socket) => {
	console.log("A person has connected! Persons now connected: "+io.eio.clientsCount);
	io.emit('newConnect', io.eio.clientsCount);
	io.emit('hereAreTheScores', highscores);
	
	socket.on('disconnect', () => {
		console.log("A person has disconnected! Persons now connected: "+io.eio.clientsCount);
		io.emit('newDisconnect', io.eio.clientsCount);
	});
		
	socket.on('setANewHighScore', (name,score) => {
		if (name==undefined || score == undefined || name=="undefined" || score == "undefined") {
			if (name==undefined || name == "undefined") {
				console.error('err: name is undefined');
			}
			
			if (score == undefined || score == "undefined") {
				console.error('err: score is undefined');
			}
			return;
		}
			
	
			
		var setScore = false;
		
		for (let i = 0; i < highscores.length; i ++) {
			if (highscores[i].name == name) {
				console.log('Found name '+name+' in highScores. Updating...');
				highscores[i].score = score;
				collection.updateOne({"name":name}, {"score":score}, function(err, res) {
					if (err) { throw err; console.log(err); }
					console.log('Updated '+name+' sucessfully.');
				});
				setScore = true;
			}
		}
		
		if (!setScore) {
			
			if (name == undefined || name == "undefined") {
				name = "420";
				while (name.toString().includes("69")||name.toString().includes("420")) {
					name = "Person #"+Math.floor(Math.random()*1000);
				}
			}
			
			console.log('Did not find person '+name+' in highScores.  Inserting...');
			
			highscores.push({
				"name": name,
				"score": score,
			});
			
			
			console.log(highscores[highscores.length-1]);
			
			collection.insert({
				"name": name,
				"score": score,
			}, function(err) {
		 		if (err) { console.log('Error inserting into collection: ' + err); }
				console.log('Inserted '+name+' sucessfully with score '+score+'.');
				console.log(name, score);
			})
			
		}
	
		io.emit('hereAreTheScores', highscores);
	});

});


/* Routing */

app.post('/die', (req, res) => {
	if (req.body) {
		if (req.body.pw == "99837") { // yes, i realize that this is a public repo.  I don't care.
			res.send(true);
			process.exit();
		} else {
			res.status(404).send('404');
		}
	}
});
	 
app.post('/getscoresagain', (req, res) => {
	getHighScores(true);
	res.header('Access-Control-Allow-Origin', '*').send(true);
});

app.get('*', (req, res) => {
	res.redirect('https://s.codetasty.com/pruvalone/mysandbox/memorizer');
});

app.post('*', (req, res) => {
	res.status('404').send('404');
});

